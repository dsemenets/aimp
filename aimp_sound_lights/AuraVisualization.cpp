#include "stdafx.h"

#include "AuraVisualization.h"
#include "PluginData.h"
#include "Colors.h"
#include "log.h"

#define avg(a,b) (((a) + (b)) / 2)
#define max3(a,b,c) (max((a), (b)), (c))

// AuraVisualization
AuraVisualization::AuraVisualization (IAIMPCore* core)
{
	aimpCore = core;
	aimpCore->AddRef();
	LoadVisualizationColor();
}

// ~AuraVisualization
AuraVisualization::~AuraVisualization ()
{
	aimpCore->Release();
	aimpCore = nullptr;
}

// Initialize
HRESULT WINAPI AuraVisualization::Initialize (int width, int height)
{
	Resize(width, height);
	InitializePaintTools();
	return S_OK;
}

// Finalize
void WINAPI AuraVisualization::Finalize()
{
	SaveVisualizationColor();
	FinalizePaintTools();
}

// GetFlags
int WINAPI AuraVisualization::GetFlags()
{
	return AIMP_VISUAL_FLAGS_RQD_DATA_WAVE | AIMP_VISUAL_FLAGS_RQD_DATA_SPECTRUM;
}

// GetMaxDisplaySize
HRESULT WINAPI AuraVisualization::GetMaxDisplaySize(int *width, int *height)
{
	*width = MAX_WIDTH;
	*height = MAX_HEIGTH;
	return S_OK;
}

HRESULT WINAPI AuraVisualization::GetName(IAIMPString **out)
{
	MakeString(VISUALIZATION_NAME, out);
	return S_OK;
}

// Click
void WINAPI AuraVisualization::Click(int x, int y, int button)
{
	if (button == AIMP_VISUAL_CLICK_BUTTON_MIDDLE)
	{
		currentColorIndex = (currentColorIndex + 1) % MAX_COLORS;
		visColor = COLOR_PALETTE [currentColorIndex];

		bgndBrush->SetColor(Color(TransformColor(visColor, DIM_BACKGROUND)));
		circleBrush->SetColor(Color(TransformColor(visColor, DIM_CIRCLE)));
	}
}

// Draw
#if 1
float prevRawData[CHANNELS];

void WINAPI AuraVisualization::Draw(HDC hdc, PAIMPVisualData data)
{
    int StepX, StepY;
    int Height, Width;
    int i, h, x;
    float* rawData;
    wchar_t outStr[100];

    OpenDevice();
    rawData = WriteDevice(data->Spectrum[2]);

    Graphics g(hdc);
    g.SetPageUnit(UnitPixel);
    Rect rect(0, 0, visSize.cx, visSize.cy);
//    g.SetClip(rect);
    g.FillRectangle(bgndBrush, rect);
    Height = visSize.cy;
    Width = visSize.cx;
    StepX = Width / CHANNELS;
    StepY = Height / 50;
    h = StepX * 2 / 3;
    x = 20;
    Gdiplus::Font dataFont(L"Arial", 6);
    for (i = 0; i < CHANNELS; i++) {
        swprintf(outStr, sizeof(outStr)/sizeof(wchar_t)- sizeof(wchar_t), L"%5.2f", rawData[i]);
        if (rawData[i] > prevRawData[i])
            prevRawData[i] = rawData[i];
        else
            prevRawData[i] -= (float)0.15;
//        g.DrawRectangle(redPen, x, (int)(Height - prevRawData[i] * StepY) - 20, h, (int)(prevRawData[i] * StepY));
        g.DrawLine(redPen, x, (int)(Height - prevRawData[i] * StepY) - 20, x + h, (int)(Height - prevRawData[i] * StepY) - 20);
        g.FillRectangle(circleBrush, x, (int)(Height - rawData[i] * StepY) - 20, h, (int)(rawData[i] * StepY));
        g.DrawRectangle(yellowPen, x, (int)(Height - rawData[i] * StepY) - 20, h, (int)(rawData[i] * StepY));
        g.DrawString(outStr, wcslen(outStr), &dataFont, PointF((float)x, (float)Height - 35), whiteBrush);
        x += StepX + 1;
    }
    Gdiplus::Font font(L"Arial", 16);
    if (IsUsbOpened())
        wcscpy(outStr, L"    Opened");
    else
        wsprintf(outStr, L" Not Opened (err %u)", errorCode);
    g.DrawString(outStr, wcslen(outStr), &font, PointF(10.0, 10.0), whiteBrush);
}
#else
void WINAPI AuraVisualization::Draw(HDC hdc, PAIMPVisualData data)
{
    int StepX, StepY;
    int Height, Width;
    int i, h, x;


    OpenDevice();
    WriteDevice(data->Spectrum[2]);

    Graphics g(hdc);
    g.SetPageUnit(UnitPixel);
    Rect rect(0, 0, visSize.cx, visSize.cy);
    //    g.SetClip(rect);
    g.FillRectangle(bgndBrush, rect);
    Height = visSize.cy;
    Width = visSize.cx;
    StepX = Width / 256;
    StepY = Height / 20;
    h = StepX / 3;
    x = 0;
    for (i = 0; i < 256; i++) {
        //        g.DrawRectangle(bluePen, x, (int)(Height - data->Spectrum[0][i] * StepY) - 20, h, (int)(data->Spectrum[0][i] * StepY));
        //        g.DrawRectangle(redPen, x + h, (int)(Height - data->Spectrum[1][i] * StepY) - 20, h, (int)(data->Spectrum[1][i] * StepY));
        g.DrawRectangle(yellowPen, x + h * 2, (int)(Height - data->Spectrum[2][i] * StepY) - 20, h, (int)(data->Spectrum[2][i] * StepY));
        x += StepX + 1;
    }
    Gdiplus::Font font(L"Arial", 16);
    g.DrawString((IsUsbOpened() ? L"    Opened" : L"Not Opened"), 10, &font, PointF(10.0, 10.0), whiteBrush);
}
#endif
// Resize
void WINAPI AuraVisualization::Resize(int newWidth, int newHeight)
{
	visSize.cx = newWidth;
	visSize.cy = newHeight;

	visCenter.x = newWidth / 2;
	visCenter.y = newHeight / 2;

	int minDimension = min (newWidth, newHeight);

	outerRadius = minDimension * OUTER_RADIUS_PERCENT / 100;
	innerRadius = minDimension * INNER_RADIUS_PERCENT / 100;

	waveAmplitude = minDimension * WAVE_AMPLITUDE_PERCENT / 100;

	waveStep = minDimension / K_WAVE_STEP + B_WAVE_STEP;
	lineStep = minDimension / K_LINE_STEP + B_LINE_STEP;
    for (int i = 0; i < 12; i++)
        prevRawData[i] = 0.0;
}

// InitializePaintTools
void AuraVisualization::InitializePaintTools()
{
	bgndBrush = new SolidBrush(Color(TransformColor(visColor, DIM_BACKGROUND)));
	circleBrush = new SolidBrush(Color(TransformColor(visColor, DIM_CIRCLE)));

	// Color is changed dynamically
	// Black color is just a stub
	dotBrush = new SolidBrush(Color::Black);
//	redPen = new Pen(Color::Black, 1.0f);
    whiteBrush = new SolidBrush(Color::White);
    bluePen = new Pen(Color::Blue, 1.0f);
    redPen = new Pen(Color::Red, 1.0f);
    yellowPen = new Pen(Color::Yellow, 1.0f);
    textPen = new Pen(Color::White, 1.0f);
}

// FinalizePaintTools
void AuraVisualization::FinalizePaintTools()
{
	delete redPen;
	delete dotBrush;
	delete bgndBrush;
	delete circleBrush;
    delete whiteBrush;
}

// LoadVisualizationColor
void AuraVisualization::LoadVisualizationColor()
{
	IAIMPServiceConfigPtr configService;
	if (SUCCEEDED(aimpCore->QueryInterface(IID_IAIMPServiceConfig, (void**)&configService)))
	{
		IAIMPStringPtr sKeyString;
		MakeString(OPTION_COLOR_INDEX, &sKeyString);

		if (FAILED(configService->GetValueAsInt32(sKeyString, &currentColorIndex)))
		{
			currentColorIndex = 0;
		}
	}

	visColor = COLOR_PALETTE[currentColorIndex];
}

// SaveVisualizationColor
void AuraVisualization::SaveVisualizationColor()
{
	IAIMPServiceConfigPtr configService;
	if (SUCCEEDED (aimpCore->QueryInterface(IID_IAIMPServiceConfig, (void**)&configService)))
	{
		IAIMPStringPtr sKeyString;
		MakeString(OPTION_COLOR_INDEX, &sKeyString);

		configService->SetValueAsInt32(sKeyString, currentColorIndex);
	}
}

// TransformColor
DWORD AuraVisualization::TransformColor(DWORD color, short light)
{
	int a = ((color & 0xFF000000) >> 24);
	int r = ((color & 0xFF0000) >> 16) + light;
	int g = ((color & 0xFF00) >> 8) + light;
	int b = (color & 0xFF) + light;

	b = min (max (0, b), 0xFF);
	g = min (max (0, g), 0xFF);
	r = min (max (0, r), 0xFF);

	return (a << 24) | (r << 16) | (g << 8) | b;
}

// MakeString
HRESULT AuraVisualization::MakeString (PWCHAR strSeq, IAIMPString** out)
{
	auto result = aimpCore->CreateObject(IID_IAIMPString, (void**)out);
	if (SUCCEEDED(result))
	{
		return (*out)->SetData(strSeq, wcslen (strSeq));
	}
	return result;
}
