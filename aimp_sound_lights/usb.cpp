#include "stdafx.h"
#include <windows.h>
#include <stdint.h>
#include <setupapi.h>
#include <hidsdi.h>
#include <math.h>
#include "PluginData.h"
#include "AIMP_SDK\apiVisuals.h"

static GUID Guid;
static wchar_t* dev_path = NULL;
HIDP_CAPS Capabilities;
static HANDLE usbDev = INVALID_HANDLE_VALUE;
float rawChannel[CHANNELS];
DWORD errorCode;
#pragma pack(push, 1)
struct channelsData {
    uint8_t repId;
    uint16_t channels[12];     // Currently devce accepts 24 bytes
} channels;
#pragma pack(pop)


static void GetDeviceCapabilities(HANDLE dev)
{
    //Get the Capabilities structure for the device.

    PHIDP_PREPARSED_DATA	PreparsedData;

    /*
    API function: HidD_GetPreparsedData
    Returns: a pointer to a buffer containing the information about the device's capabilities.
    Requires: A handle returned by CreateFile.
    There's no need to access the buffer directly,
    but HidP_GetCaps and other API functions require a pointer to the buffer.
    */

    if (!HidD_GetPreparsedData(dev, &PreparsedData))
        return;
    //DisplayLastError("HidD_GetPreparsedData: ");

    /*
    API function: HidP_GetCaps
    Learn the device's capabilities.
    For standard devices such as joysticks, you can find out the specific
    capabilities of the device.
    For a custom device, the software will probably know what the device is capable of,
    and the call only verifies the information.
    Requires: the pointer to the buffer returned by HidD_GetPreparsedData.
    Returns: a Capabilities structure containing the information.
    */

    (void)HidP_GetCaps(PreparsedData, &Capabilities);
    //DisplayLastError("HidP_GetCaps: ");

    //Display the capabilities

    /*
    printf("%s%X\n", "Usage Page:                      ", Capabilities.UsagePage);
    printf("%s%d\n", "Input Report Byte Length:        ", Capabilities.InputReportByteLength);
    printf("%s%d\n", "Output Report Byte Length:       ", Capabilities.OutputReportByteLength);
    printf("%s%d\n", "Feature Report Byte Length:      ", Capabilities.FeatureReportByteLength);
    printf("%s%d\n", "Number of Link Collection Nodes: ", Capabilities.NumberLinkCollectionNodes);
    printf("%s%d\n", "Number of Input Button Caps:     ", Capabilities.NumberInputButtonCaps);
    printf("%s%d\n", "Number of InputValue Caps:       ", Capabilities.NumberInputValueCaps);
    printf("%s%d\n", "Number of InputData Indices:     ", Capabilities.NumberInputDataIndices);
    printf("%s%d\n", "Number of Output Button Caps:    ", Capabilities.NumberOutputButtonCaps);
    printf("%s%d\n", "Number of Output Value Caps:     ", Capabilities.NumberOutputValueCaps);
    printf("%s%d\n", "Number of Output Data Indices:   ", Capabilities.NumberOutputDataIndices);
    printf("%s%d\n", "Number of Feature Button Caps:   ", Capabilities.NumberFeatureButtonCaps);
    printf("%s%d\n", "Number of Feature Value Caps:    ", Capabilities.NumberFeatureValueCaps);
    printf("%s%d\n", "Number of Feature Data Indices:  ", Capabilities.NumberFeatureDataIndices);
    */
    //No need for PreparsedData any more, so free the memory it's using.

    HidD_FreePreparsedData(PreparsedData);

    //DisplayLastError("HidD_FreePreparsedData: ") ;
}

static char* FromUnicode(char* unicode)
{
    char* src = unicode;
    char* dst = unicode;
    while (*src != 0)
    {
        *dst++ = *src;
        src += 2;
    }
    *dst = 0;
    return unicode;
}

void OpenDevice()
{
    HDEVINFO info;

    if (usbDev != INVALID_HANDLE_VALUE)
        return;
    errorCode = 0;
    HidD_GetHidGuid(&Guid);
    __try
    {
        __try
        {

            info = SetupDiGetClassDevs(&Guid, NULL, NULL, DIGCF_PRESENT | DIGCF_INTERFACEDEVICE);
            if (info != INVALID_HANDLE_VALUE)
            {
                DWORD devIndex;
                SP_INTERFACE_DEVICE_DATA ifData;
                ifData.cbSize = sizeof(ifData);

                for (devIndex = 0; SetupDiEnumDeviceInterfaces(info, NULL, &Guid, devIndex, &ifData); ++devIndex)
                {
                    DWORD needed;

                    SetupDiGetDeviceInterfaceDetail(info, &ifData, NULL, 0, &needed, NULL);

                    PSP_INTERFACE_DEVICE_DETAIL_DATA detail = (PSP_INTERFACE_DEVICE_DETAIL_DATA)malloc(needed);
                    if (detail != NULL)
                    {
                        detail->cbSize = sizeof(SP_INTERFACE_DEVICE_DETAIL_DATA);
                        SP_DEVINFO_DATA did = { sizeof(SP_DEVINFO_DATA) };

                        if (SetupDiGetDeviceInterfaceDetail(info, &ifData, detail, needed, NULL, &did))
                        {
                            // Add the link to the list of all HID devices
                            if (wcsstr(detail->DevicePath, L"vid_1610") != NULL)
                            {
                                if (dev_path != NULL)
                                    free(dev_path);
                                dev_path = (wchar_t*)malloc((wcslen(detail->DevicePath) + 1) * sizeof(wchar_t));
                                wcscpy(dev_path, detail->DevicePath);
                            }
                        }
                        free(detail);
                    }
                }
                SetupDiDestroyDeviceInfoList(info);
            }

            if (dev_path != NULL)
            {
                usbDev = CreateFile(dev_path, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL);
                if (usbDev != INVALID_HANDLE_VALUE)
                {
                    /*
                    HIDD_ATTRIBUTES Attributes;// = (PHIDD_ATTRIBUTES)malloc(sizeof(HIDD_ATTRIBUTES));

                    if (HidD_GetManufacturerString(usbDev, device, 252))
                        printf("Manufacturer %s\n", FromUnicode(device));
                    if (HidD_GetProductString(usbDev, device, 252))
                        printf("Product %s\n", FromUnicode(device));
                    Attributes.Size = sizeof(HIDD_ATTRIBUTES);
                    if (HidD_GetAttributes(usbDev, &Attributes))
                    {
                        printf("Device %04X:%04X\n", Attributes.VendorID, Attributes.ProductID);
                        printf("Version %d\n", Attributes.VersionNumber);
                    }
                    */
                    GetDeviceCapabilities(usbDev);
                    errorCode = 0;
                }
                else
                {
                    errorCode = GetLastError();
                }

            }
        }
        __except (EXCEPTION_EXECUTE_HANDLER)
        {
            if (dev_path != NULL)
                free(dev_path);
            usbDev = INVALID_HANDLE_VALUE;
            dev_path = NULL;
        }
    }
    __finally
    {
        if (dev_path != NULL)
            free(dev_path);
        dev_path = NULL;
    }
}

void ChannelData(int channel, TAIMPVisualDataSpectrum inData, int start, int end)
{
    float temp;
    rawChannel[channel] = 0.0;
    for (int i = start; i <= end; i++)
    {
        temp = inData[i];
        if (rawChannel[channel] < temp)
            rawChannel[channel] = temp;
    }
}

void GetSpectr(TAIMPVisualDataSpectrum inData)
{
#ifdef LOGARITHMIC
    // Logarithmic scale
#if CHANNELS == 12
    ChannelData(0, inData, 0, 0);
    ChannelData(1, inData, 1, 1);
    ChannelData(2, inData, 2, 2);
    ChannelData(3, inData, 3, 5);
    ChannelData(4, inData, 6, 9);
    ChannelData(5, inData, 10, 14);
    ChannelData(6, inData, 15, 24);
    ChannelData(7, inData, 25, 39);
    ChannelData(8, inData, 40, 62);
    ChannelData(9, inData, 63, 100);
    ChannelData(10, inData, 101, 160);
    ChannelData(11, inData, 161, 255);
#elif CHANNELS == 6
    ChannelData(0, inData, 0, 1);
    ChannelData(1, inData, 2, 5);
    ChannelData(2, inData, 6, 14);
    ChannelData(3, inData, 15, 39);
    ChannelData(4, inData, 40, 100);
    ChannelData(5, inData, 101, 255);
#else
#error "Unsupported number of channels"
#endif
#else
    // Linear scale
    ChannelData(0, inData, 0, 20);
    ChannelData(1, inData, 21, 41);
    ChannelData(2, inData, 42, 63);
    ChannelData(3, inData, 64, 84);
    ChannelData(4, inData, 85, 105);
    ChannelData(5, inData, 106, 127);
    ChannelData(6, inData, 128, 148);
    ChannelData(7, inData, 149, 169);
    ChannelData(8, inData, 170, 191);
    ChannelData(9, inData, 192, 212);
    ChannelData(10, inData, 213, 233);
    ChannelData(11, inData, 324, 255);
#endif
    for (int i = 0; i < CHANNELS; i++)
    {
//        rawChannel[i] /= 150.0;
        channels.channels[i] = (uint16_t)(4095.0 * pow(rawChannel[i]/150.0, 0.5));
        if (channels.channels[i] > 4095)
            channels.channels[i] = 4095;
    }
}

float *WriteDevice(TAIMPVisualDataSpectrum inData)
{
//    uint8_t data[LEDS_NUM];
//    int i, j;
    int num = 0;

    GetSpectr(inData);
    if (usbDev != INVALID_HANDLE_VALUE)
    {
        channels.repId = 0;
        __try
        {
            if (!HidD_SetOutputReport(usbDev, &channels, Capabilities.OutputReportByteLength))
            {
                errorCode = GetLastError();
                CloseHandle(usbDev);
                usbDev = INVALID_HANDLE_VALUE;
            }
        }
        __except (EXCEPTION_EXECUTE_HANDLER)
        {
            usbDev = INVALID_HANDLE_VALUE;
        }
    }
    return rawChannel;
}

bool IsUsbOpened()
{
    return usbDev != INVALID_HANDLE_VALUE;
}
