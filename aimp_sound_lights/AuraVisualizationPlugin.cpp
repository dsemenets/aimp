#include "stdafx.h"

#include "AuraVisualizationPlugin.h"
#include "PluginData.h"
#include <stdio.h>

FILE *file;

HRESULT WINAPI AuraVisualizationPlugin::Initialize (IAIMPCore* core)
{
	FILE *file;
	file = fopen("c:\\Users\\dsemenets\\log.txt", "w");
	if (!IsServiceAvailable (core, IID_IAIMPServiceVisualizations))
	{
		if (file)
			fprintf(file, "returned E_FAIL\n");
		return E_FAIL;
	}

	if (file)
		fprintf(file, "Initialisation\n");

	GdiplusStartupInput gdiplusStartupInput;
	GdiplusStartup (&gdiplusToken, &gdiplusStartupInput, NULL);

	visualization = new AuraVisualization (core);
	visualization->AddRef ();
	core->RegisterExtension (IID_IAIMPServiceVisualizations, visualization);
	if (file)
		fprintf(file, "%s returns S_OK\n", __func__);
	return S_OK;
}
// Initialize

HRESULT WINAPI AuraVisualizationPlugin::Finalize ()
{
	visualization->Release ();
	visualization = nullptr;
	if (file)
		fprintf(file, "Finalisation\n");

	GdiplusShutdown (gdiplusToken);
	return S_OK;
}
// Finalize

PWCHAR WINAPI AuraVisualizationPlugin::InfoGet (int index)
{
	switch (index)
	{
		case AIMP_PLUGIN_INFO_NAME: return PLUGIN_NAME;
		case AIMP_PLUGIN_INFO_AUTHOR: return PLUGIN_AUTHOR;
		case AIMP_PLUGIN_INFO_SHORT_DESCRIPTION: return PLUGIN_SHORT_DESC;
		case AIMP_PLUGIN_INFO_FULL_DESCRIPTION: return PLUGIN_FULL_DESC;
		default: return nullptr;
	}
}
// InfoGet

DWORD WINAPI AuraVisualizationPlugin::InfoGetCategories ()
{
	return AIMP_PLUGIN_CATEGORY_VISUALS;
}
// InfoGetCategories

void WINAPI AuraVisualizationPlugin::SystemNotification (int notifyId, IUnknown* data)
{

}
// SystemNotification

bool AuraVisualizationPlugin::IsServiceAvailable (IUnknown* provider, REFIID serviceIid)
{
	IUnknown* dummyService;
	if (SUCCEEDED (provider->QueryInterface (serviceIid, (void**)&dummyService)))
	{
		dummyService->Release ();
		return true;
	}
	return false;
}
// IsServiceAvailable