#pragma once

const PWCHAR PLUGIN_NAME = L"SoundLights";
const PWCHAR PLUGIN_AUTHOR = L"dsemenets";
const PWCHAR PLUGIN_SHORT_DESC = L"";
const PWCHAR PLUGIN_FULL_DESC = L"";

const PWCHAR VISUALIZATION_NAME = L"Sound Lights";

const PWCHAR OPTION_COLOR_INDEX = L"SoundLights\\ColorIndex";

#define CHANNELS 6
#define LEDS_NUM 144
#define LOGARITHMIC
