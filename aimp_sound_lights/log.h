#pragma once
#include <stdio.h>

extern FILE *file;

#define LOG(fmt, ...) if (file) fprintf(file, fmt, __VA_ARGS__)
