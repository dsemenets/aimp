#include "stdafx.h"
#include "AuraVisualizationPlugin.h"
#include <stdio.h>

extern FILE * file;

bool APIENTRY DllMain (HMODULE hModule, DWORD  dwReasonForCall, LPVOID lpReserved)
{
	if (file)
		fprintf(file, "%s(%d)\n", __func__, dwReasonForCall);
	switch (dwReasonForCall)
	{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH: break;
	}
	return true;
}
// DllMain

bool WINAPI AIMPPluginGetHeader (IAIMPPlugin **header)
{
	if (file)
		fprintf(file, "%s\n", __func__);
	auto plugin = new AuraVisualizationPlugin ();
	plugin->AddRef ();
	*header = plugin;
	return true;
}
// AIMPPluginGetHeader